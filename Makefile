# BMS 2016/2017 Projekt 2
# Matej Vido, xvidom00@stud.fit.vutbr.cz
# 12/2016

CXX=g++
DEBUG_FLAGS=
#DEBUG_FLAGS=-Wall -Wextra -pedantic -g -DBMS2_DEBUG
CXX_FLAGS=-std=c++11
ZIPNAME=xvidom00.zip

all: bms2

bms2: bms2.o params.o input.o output.o packet.o stream.o
	$(CXX) $(CXX_FLAGS) $(DEBUG_FLAGS) $^ -o $@

bms2.o: bms2.cpp bms2.h
	$(CXX) $(CXX_FLAGS) $(DEBUG_FLAGS) -c $< -o $@

params.o: params.cpp bms2.h
	$(CXX) $(CXX_FLAGS) $(DEBUG_FLAGS) -c $< -o $@

input.o: input.cpp bms2.h
	$(CXX) $(CXX_FLAGS) $(DEBUG_FLAGS) -c $< -o $@

output.o: output.cpp bms2.h
	$(CXX) $(CXX_FLAGS) $(DEBUG_FLAGS) -c $< -o $@

packet.o: packet.cpp bms2.h
	$(CXX) $(CXX_FLAGS) $(DEBUG_FLAGS) -c $< -o $@

stream.o: stream.cpp bms2.h
	$(CXX) $(CXX_FLAGS) $(DEBUG_FLAGS) -c $< -o $@

.PHONY: clean purge zip

clean:
	rm -f *.o

purge: clean
	rm -f bms2

zip:
	zip $(ZIPNAME) Makefile *.cpp *.h
