/*
 * @file
 * @brief BMS 2016/2017 Projekt 2
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date 12/2016
 *
 * Demultiplexing a analyza transportneho streamu vysielania digitalnej
 * televizie.
 */

/* Global and system headers */
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <cstdint>
#include <map>

/* Local headers */
#include "bms2.h"

using namespace bms2;

std::string bms2::bandwidth_table[8] = {
    "8 MHz", "7 MHz", "6 MHz", "5 MHz", "reserved", "reserved", "reserved", "reserved"
};

std::string bms2::constellation_table[4] = {
    "QPSK", "16-QAM", "64-QAM", "reserved"
};

std::string bms2::code_rate_table[8] = {
    "1/2", "2/3", "3/4", "5/6", "7/8", "reserved", "reserved", "reserved"
};

std::string bms2::guard_interval_table[4] = {
    "1/32", "1/16", "1/8", "1/4"
};

/* [constellation][code_rate][guard_interval] */
double bms2::bandwidth_8MHz_table[4][8][4] =
    {
        {
            {  6.032,  5.855,  5.529,  4.976 },
            {  8.043,  7.806,  7.373,  6.635 },
            {  9.048,  8.782,  8.294,  7.465 },
            { 10.053,  9.758,  9.216,  8.294 },
            { 10.556, 10.246,  9.676,  8.709 },
        },
        {
            { 12.064, 11.709, 11.059, 9.953  },
            { 16.086, 15.612, 14.745, 13.271 },
            { 18.096, 17.564, 16.588, 14.929 },
            { 20.107, 19.516, 18.431, 16.588 },
            { 21.112, 20.491, 19.353, 17.418 },
        },
        {
            { 18.096, 17.564, 16.588, 14.929 },
            { 24.128, 23.419, 22.118, 19.906 },
            { 27.144, 26.346, 24.882, 22.394 },
            { 30.160, 29.273, 27.647, 24.882 },
            { 31.668, 30.737, 29.029, 26.126 },
        },
        { 0.0, }
    };

int
main(int argc, char *argv[])
{
    try {
        Params p{argc, argv};
        Stream stream{p.in_fn, p.out_fn};

        stream.find_pat();
        stream.find_nit();
        stream.find_sdt();
        stream.find_pmts();
        stream.get_pid_stats();
        stream.get_program_infos();

#ifdef BMS2_DEBUG
        for (auto& program: stream.pat.programs) {
            BMS2_DEBUG_MSG("Number: %u", program.number);
            BMS2_DEBUG_MSG("   PID: %u", program.pid);
        }
        for (auto& pid: stream.pid_stats) {
            BMS2_DEBUG_MSG("PID: %u VALUE: %lu", pid.first, pid.second);
        }
        for (auto& s: stream.sdt.services) {
            BMS2_DEBUG_MSG("Service ID: %u", s.first);
            BMS2_DEBUG_MSG("  provider: %s", s.second.provider.c_str());
            BMS2_DEBUG_MSG("  name:     %s", s.second.name.c_str());
        }
        for (auto& pmt: stream.pmts) {
            BMS2_DEBUG_MSG("PMT PID: %u", pmt.first);
            for (auto& pid: pmt.second.pids) {
                BMS2_DEBUG_MSG("  PID: %u", pid);
            }
        }
#endif

        stream.print_header();
        stream.print_program_infos();

        BMS2_DEBUG_MSG("Packets count: %lu", stream.packets_count);
    } catch (Error &e) {
        std::cerr << e.what() << std::endl;
        return e.ecode;
    } catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
        return EANY;
    }

    return EOK;
}
