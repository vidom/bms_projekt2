/*
 * @file
 * @brief BMS 2016/2017 Projekt 2
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date 12/2016
 */

/* Local header */
#include "bms2.h"

using namespace bms2;

const std::string Params::in_suffix = ".ts";
const std::string Params::out_suffix = ".txt";

/*
 * @brief Params constructor.
 */
Params::Params(int argc, char *argv[]) :
    in_fn{},
    fn{},
    out_fn{}
{
    if (argc != 2) {
        print_help(argv);
        throw Error{"Wrong arguments!", EARG};
    }

    this->in_fn = argv[1];
    BMS2_DEBUG_MSG("Input filename: %s", this->in_fn.c_str());

    if (this->in_fn.length() < Params::in_suffix.length() ||
            this->in_fn.compare(this->in_fn.length() - Params::in_suffix.length(), std::string::npos, Params::in_suffix)) {
        print_help(argv);
        throw Error{"Wrong format of input file's filename!", EINFN};
    }

    this->fn = this->in_fn.substr(0, this->in_fn.length() - Params::in_suffix.length());
    BMS2_DEBUG_MSG("Filename: %s", this->fn.c_str());
    this->out_fn = this->fn + Params::out_suffix;
    BMS2_DEBUG_MSG("Output filename: %s", this->out_fn.c_str());
}

/*
 * @brief Prints help message.
 */
void
Params::print_help(char *argv[])
{
    std::cerr << "BMS 2016/2017 Project 2" << std::endl;
    std::cerr << "Demultiplexing and analysis of DVB transport stream." << std::endl;
    std::cerr << "Usage:" << std::endl;
    std::cerr << "    " << argv[0] << " <filename>" << Params::in_suffix << std::endl;
    std::cerr << "        <filename>" << Params::in_suffix << " Filename of input file." << std::endl;
    std::cerr << "                      Expected format of filename is *" << Params::in_suffix << std::endl;
    std::cerr << std::endl;
    std::cerr << "Output is in file: <filename>" << Params::out_suffix << std::endl;
}
