#! /bin/bash

FILE=multiplex.zip

wget -O $FILE --no-check-certificate https://www.fit.vutbr.cz/study/courses/BMS/public/proj2016/multiplex.zip
RET=$?
if [ $RET != "0" ]
then
    echo "Return code: $RET"
    exit 1
fi

unzip $FILE
RET=$?
if [ $RET != "0" ]
then
    echo "Return code: $RET"
    exit 1
fi
