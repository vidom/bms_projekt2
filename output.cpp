/*
 * @file
 * @brief BMS 2016/2017 Projekt 2
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date 12/2016
 */

/* Local header */
#include "bms2.h"

using namespace bms2;

/*
 * @brief Output contructor.
 */
Output::Output(std::string fn) :
    filename{fn},
    file{fn}
{
    if (!file.is_open()) {
        throw Error{"Could not open output file: " + fn, EOFILE};
    }
    BMS2_DEBUG_MSG("Output file successfully opened!");
}
