/*
 * @file
 * @brief BMS 2016/2017 Projekt 2
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date 12/2016
 */

/* Global headers */
#include <iomanip>
#include <ios>
#include <iostream>
#include <sstream>

/* Local header */
#include "bms2.h"

using namespace bms2;

/*
 * @brief Stream constructor.
 */
Stream::Stream(std::string in_fn, std::string out_fn) :
    input{in_fn},
    output{out_fn},
    nit_pid{0x0010},
    pat{},
    nit{},
    sdt{},
    pmts{},
    pid_stats{},
    packets_count{0},
    packet{},
    header_printed{false}
{
}

/*
 * @brief Reads next packet from input file.
 * @return true If whole packet was read.
 *         false If the end of file was reached and only the part of packet was read.
 */
bool
Stream::read_packet(void)
{
    this->input.file.read((char *)(this->packet.bytes), BMS2_PACKET_LENGTH);
    if (!this->input.file) {
        if (this->input.file.eof()) {
            BMS2_DEBUG_MSG("Last packet size: %ld B", this->input.file.gcount());
            return false;
        } else {
            throw Error{"Reading file failed! Expected " + std::to_string(BMS2_PACKET_LENGTH) + " B, read " + std::to_string(this->input.file.gcount()) + " B!", EIFILE};
        }
    }
    return true;
}

/*
 * @brief Finds first PAT table and parses information.
 */
void
Stream::find_pat(void)
{
    if (this->pat.section.size() > 0) {
        /* PAT is already found */
        return;
    }
    this->input.reset();
    while (this->read_packet()) {
        this->packet.parse_header();
        if (this->packet.header.pid == 0x0000) {
            if (this->packet.header.pusi == 0x01 && this->pat.section.size() > 0) {
                /* PAT is already found */
                break;
            }
            /* First starting PAT packet or continuing packet */
            uint16_t offset;
            if (this->packet.get_payload_offset(offset) == false) {
                continue;
            }
            /* Accumulate sections from all packets */
            this->pat.section.insert(this->pat.section.end(), &(this->packet.bytes[offset]), &(this->packet.bytes[offset]) + BMS2_PACKET_LENGTH - offset);
        }
    }
    if (this->pat.section.size() == 0) {
        throw Error{"PAT not found!", EIFILE};
    }

    uint16_t offset = 0;
    uint16_t pointer_field = this->pat.section[offset];
    offset++;
    offset += pointer_field;
    if (offset >= this->pat.section.size() ||
            offset + 3U > this->pat.section.size()) {
        /* Should not happen */
        BMS2_DEBUG_MSG("Section size: %lu section start: %u", this->pat.section.size(), offset);
        throw Error{"PAT section starts outside of section!!!", EIFILE};
    }

    //uint8_t table_id = this->pat.section[offset];
    offset++;
    uint16_t section_length = BMS2_HOST_TO_BIG16(&this->pat.section[offset]) & 0x03ff;
    offset += 2;
    uint16_t whole_section_length = offset + section_length;
    if (whole_section_length > this->pat.section.size()) {
        /* Should not happen */
        BMS2_DEBUG_MSG("Section size: %lu section end: %u", this->pat.section.size(), whole_section_length);
        throw Error{"PAT section ends outside of section!!!", EIFILE};
    }
    /* CRC32 (4 bytes) is in the end of section */
    uint16_t end_offset = whole_section_length - 4;
    offset += 5;
    while (offset < end_offset) {
        Program p;
        p.number = BMS2_HOST_TO_BIG16(&this->pat.section[offset]);
        offset += 2;
        p.pid = BMS2_HOST_TO_BIG16(&this->pat.section[offset]) & 0x1fff;
        offset += 2;
        this->pat.programs.push_back(p);
    }
    if (this->pat.programs.size() > 0) {
        this->nit_pid = this->pat.programs[0].pid;
    }
}

/*
 * @brief Finds first NIT table and parses information.
 */
void
Stream::find_nit(void)
{
    if (this->nit.section.size() > 0) {
        /* NIT is already found */
        return;
    }
    this->input.reset();
    while (this->read_packet()) {
        this->packet.parse_header();
        if (this->packet.header.pid == this->nit_pid) {
            if (this->packet.header.pusi == 0x01 && this->nit.section.size() > 0) {
                /* NIT is already found */
                break;
            }
            /* First starting NIT packet or continuing packet */
            uint16_t offset;
            if (this->packet.get_payload_offset(offset) == false) {
                continue;
            }
            /* Accumulate sections from all packets */
            this->nit.section.insert(this->nit.section.end(), &(this->packet.bytes[offset]), &(this->packet.bytes[offset]) + BMS2_PACKET_LENGTH - offset);
        }
    }
    if (this->nit.section.size() == 0) {
        throw Error{"NIT not found!", EIFILE};
    }

    uint16_t offset = 0;
    uint16_t pointer_field = this->nit.section[offset];
    offset++;
    offset += pointer_field;
    if (offset >= this->nit.section.size() ||
            offset + 3U > this->nit.section.size()) {
        /* Should not happen */
        BMS2_DEBUG_MSG("Section size: %lu section start: %u", this->nit.section.size(), offset);
        throw Error{"NIT section starts outside of section!!!", EIFILE};
    }

    //uint8_t table_id = this->nit.section[offset];
    offset++;
    uint16_t section_length = BMS2_HOST_TO_BIG16(&this->nit.section[offset]) & 0x03ff;
    offset += 2;
    uint16_t whole_section_length = offset + section_length;
    if (whole_section_length > this->nit.section.size()) {
        /* Should not happen */
        BMS2_DEBUG_MSG("Section size: %lu section end: %u", this->nit.section.size(), whole_section_length);
        throw Error{"NIT section ends outside of section!!!", EIFILE};
    }

    this->nit.network_id = BMS2_HOST_TO_BIG16(&this->nit.section[offset]);
    offset += 5;

    //std::cerr << "PID: " << this->packet.header.pid << std::endl;
    //std::cerr << "  section_length: " << section_length << " (0x" << std::hex << section_length << std::dec << ")" << std::endl;
    //std::cerr << "  network_id: " << this->nit.network_id << " (0x" << std::hex << this->nit.network_id << std::dec << ")" << std::endl;

    uint16_t network_descriptors_length = BMS2_HOST_TO_BIG16(&this->nit.section[offset]) & 0x0fff;
    offset += 2;
    uint16_t end_offset = offset + network_descriptors_length;
    bool name_set = false;
    while (offset < end_offset) {
        Descriptor d;
        d.tag = this->nit.section[offset];
        offset++;
        d.length = this->nit.section[offset];
        offset++;
        //std::cerr << "    tag: " << int(d.tag) << " (0x" << std::hex << int(d.tag) << std::dec << ")" << std::endl;
        //std::cerr << "    length: " << int(d.length) << " (0x" << std::hex << int(d.length) << std::dec << ")" << std::endl;
        if (d.tag == 0x40) {
            /* 0x40 ... tag value for network_name_descriptor */
            std::string name = "";
            for (int i = 0; i < d.length; i++) {
                name += this->nit.section[offset];
                offset++;
            }
            if (name_set == false) {
                this->nit.network_name = name;
                name_set = true;
            }
        } else {
            offset += d.length;
        }
    }

    uint16_t transport_stream_loop_length = BMS2_HOST_TO_BIG16(&this->nit.section[offset]) & 0x0fff;
    offset += 2;
    end_offset = offset + transport_stream_loop_length;
    bool values_set = false;
    while (offset < end_offset) {
        //uint16_t transport_stream_id = BMS2_HOST_TO_BIG16(&this->nit.section[offset]);
        offset += 2;
        //uint16_t original_network_id = BMS2_HOST_TO_BIG16(&this->nit.section[offset]);
        offset += 2;
        uint16_t transport_descriptors_length = BMS2_HOST_TO_BIG16(&this->nit.section[offset]) & 0x0fff;
        offset += 2;
        uint16_t inner_end_offset = offset + transport_descriptors_length;
        while (offset < inner_end_offset) {
            Descriptor d;
            d.tag = this->nit.section[offset];
            offset++;
            d.length = this->nit.section[offset];
            offset++;
            //std::cerr << "    tag: " << int(d.tag) << " (0x" << std::hex << int(d.tag) << std::dec << ")" << std::endl;
            //std::cerr << "    length: " << int(d.length) << " (0x" << std::hex << int(d.length) << std::dec << ")" << std::endl;
            if (d.tag == 0x5a) {
                /* 0x5a ... tag value for terrestrial_delivery_system_descriptor */
                offset += 4;
                uint8_t bandwidth = (this->nit.section[offset] & 0xe0) >> 5;
                offset++;
                uint8_t constellation = (this->nit.section[offset] & 0xc0) >> 6;
                uint8_t code_rate_hp_stream = (this->nit.section[offset] & 0x07);
                offset++;
                uint8_t guard_interval = (this->nit.section[offset] & 0x18) >> 3;
                offset++;
                offset += 4;
                //std::cerr << "      bandwidth: " << bandwidth_table[bandwidth] << std::endl;
                //std::cerr << "      constellation: " << constellation_table[constellation] << std::endl;
                //std::cerr << "      guard_interval: " << guard_interval_table[guard_interval] << std::endl;
                //std::cerr << "      code_rate: " << code_rate_table[code_rate_hp_stream] << std::endl;
                if (values_set == false) {
                    this->nit.bandwidth = bandwidth_table[bandwidth];
                    this->nit.constellation = constellation_table[constellation];
                    this->nit.guard_interval = guard_interval_table[guard_interval];
                    this->nit.code_rate = code_rate_table[code_rate_hp_stream];
                    this->nit.bandwidth_num = bandwidth;
                    this->nit.constellation_num = constellation;
                    this->nit.guard_interval_num = guard_interval;
                    this->nit.code_rate_num = code_rate_hp_stream;
                }
            } else {
                offset += d.length;
            }
        }
    }
}

/*
 * @brief Finds first SDT table and parses information.
 */
void
Stream::find_sdt(void)
{
    if (this->sdt.section.size() > 0) {
        /* SDT is already found */
        return;
    }
    this->input.reset();
    while (this->read_packet()) {
        this->packet.parse_header();
        if (this->packet.header.pid == 0x0011) {
            if (this->packet.header.pusi == 0x01 && this->sdt.section.size() > 0) {
                /* SDT is already found */
                break;
            }
            /* First starting SDT packet or continuing packet */
            uint16_t offset;
            if (this->packet.get_payload_offset(offset) == false) {
                continue;
            }
            /* Accumulate sections from all packets */
            this->sdt.section.insert(this->sdt.section.end(), &(this->packet.bytes[offset]), &(this->packet.bytes[offset]) + BMS2_PACKET_LENGTH - offset);
        }
    }
    if (this->sdt.section.size() == 0) {
        throw Error{"SDT not found!", EIFILE};
    }

    uint16_t offset = 0;
    uint16_t pointer_field = this->sdt.section[offset];
    offset++;
    offset += pointer_field;
    if (offset >= this->sdt.section.size() ||
            offset + 3U > this->sdt.section.size()) {
        /* Should not happen */
        BMS2_DEBUG_MSG("Section size: %lu section start: %u", this->sdt.section.size(), offset);
        throw Error{"SDT section starts outside of section!!!", EIFILE};
    }

    //uint8_t table_id = this->sdt.section[offset];
    offset++;
    uint16_t section_length = BMS2_HOST_TO_BIG16(&this->sdt.section[offset]) & 0x03ff;
    offset += 2;
    uint16_t whole_section_length = offset + section_length;
    if (whole_section_length > this->sdt.section.size()) {
        /* Should not happen */
        BMS2_DEBUG_MSG("Section size: %lu section end: %u", this->sdt.section.size(), whole_section_length);
        throw Error{"SDT section ends outside of section!!!", EIFILE};
    }
    /* CRC32 (4 bytes) is in the end of section */
    uint16_t end_offset = whole_section_length - 4;
    offset += 8;
    while (offset < end_offset) {
        Service s;
        uint16_t id = BMS2_HOST_TO_BIG16(&this->sdt.section[offset]);
        offset += 2;
        offset++;
        uint16_t descriptors_loop_length = BMS2_HOST_TO_BIG16(&this->sdt.section[offset]) & 0x0fff;
        offset += 2;
        uint16_t inner_end_offset = offset + descriptors_loop_length;
        bool service_set = false;
        while (offset < inner_end_offset) {
            Descriptor d;
            d.tag = this->sdt.section[offset];
            offset++;
            d.length = this->sdt.section[offset];
            offset++;
            //std::cerr << "    tag: " << int(d.tag) << " (0x" << std::hex << int(d.tag) << std::dec << ")" << std::endl;
            //std::cerr << "    length: " << int(d.length) << " (0x" << std::hex << int(d.length) << std::dec << ")" << std::endl;
            if (d.tag == 0x48) {
                offset++;
                std::string service_provider = "";
                uint8_t service_provider_name_length = this->sdt.section[offset];
                offset++;
                for (int i = 0; i < service_provider_name_length; i++) {
                    service_provider += this->sdt.section[offset];
                    offset++;
                }
                std::string service_name = "";
                uint8_t service_name_length = this->sdt.section[offset];
                offset++;
                for (int i = 0; i < service_name_length; i++) {
                    service_name += this->sdt.section[offset];
                    offset++;
                }
                if (service_set == false) {
                    s.provider = service_provider;
                    s.name = service_name;
                    service_set = true;
                }
            } else {
                offset += d.length;
            }
        }
        this->sdt.services[id] = s;
    }
}

/*
 * @brief Finds first occurence of all PMT tables and parses information.
 */
void
Stream::find_pmts(void)
{
    if (this->pmts.size() > 0) {
        /* PMTs are already found */
        return;
    }
    for (auto program: this->pat.programs) {
        if (program.pid == this->nit_pid) {
            /* Skip NIT table - PID of NIT is also in this list of programs */
            continue;
        }
        PMT pmt;
        this->input.reset();
        while (this->read_packet()) {
            this->packet.parse_header();
            if (this->packet.header.pid == program.pid) {
                if (this->packet.header.pusi == 0x01 && pmt.section.size() > 0) {
                    /* PMT is already found */
                    break;
                }
                /* First starting PMT packet or continuing packet */
                uint16_t offset;
                if (this->packet.get_payload_offset(offset) == false) {
                    continue;
                }
                /* Accumulate sections from all packets */
                pmt.section.insert(pmt.section.end(), &(this->packet.bytes[offset]), &(this->packet.bytes[offset]) + BMS2_PACKET_LENGTH - offset);
            }
        }
        if (pmt.section.size() == 0) {
            throw Error{"PMT not found!", EIFILE};
        }
 
        uint16_t offset = 0;
        uint16_t pointer_field = pmt.section[offset];
        offset++;
        offset += pointer_field;
        if (offset >= pmt.section.size() ||
                offset + 3U > pmt.section.size()) {
            /* Should not happen */
            BMS2_DEBUG_MSG("Section size: %lu section start: %u", pmt.section.size(), offset);
            throw Error{"PMT section starts outside of section!!!", EIFILE};
        }

        //uint8_t table_id = pmt.section[offset];
        offset++;
        uint16_t section_length = BMS2_HOST_TO_BIG16(&pmt.section[offset]) & 0x03ff;
        offset += 2;
        uint16_t whole_section_length = offset + section_length;
        if (whole_section_length > pmt.section.size()) {
            /* Should not happen */
            BMS2_DEBUG_MSG("Section size: %lu section end: %u", pmt.section.size(), whole_section_length);
            throw Error{"PMT section ends outside of section!!!", EIFILE};
        }
        offset += 7;
        uint16_t program_info_length = BMS2_HOST_TO_BIG16(&pmt.section[offset]) & 0x0fff;
        offset += 2;
        offset += program_info_length;
        /* CRC32 (4 bytes) is in the end of section */
        uint16_t end_offset = whole_section_length - 4;

        //std::cerr << "PMT PID: " << program.pid << std::endl;
        //std::cerr << "  section_length: " << section_length << std::endl;
        //std::cerr << "  whole_section_length: " << whole_section_length << std::endl;
        //std::cerr << "  program_info_length: " << program_info_length << std::endl;
        //std::cerr << "  end_offset: " << end_offset << std::endl;

        while (offset < end_offset) {
            offset++;
            uint16_t pid = BMS2_HOST_TO_BIG16(&pmt.section[offset]) & 0x1fff;
            offset += 2;
            uint16_t es_info_length = BMS2_HOST_TO_BIG16(&pmt.section[offset]) & 0x0fff;
            offset += 2;
            offset += es_info_length;

            //std::cerr << "    pid: " << pid << std::endl;
            //std::cerr << "    es_info_length: " << es_info_length << std::endl;
            //std::cerr << "    offset: " << offset << std::endl;

            pmt.pids.push_back(pid);
        }
        this->pmts[program.pid] = pmt;
    }
}

/*
 * @brief Counts number of packets with every PID.
 */
void
Stream::get_pid_stats(void)
{
    if (this->pid_stats.size() > 0) {
        /* PID stats already found */
        return;
    }
    this->input.reset();
    while (this->read_packet()) {
        this->packet.parse_header();
        this->packets_count++;
        if (this->pid_stats.count(this->packet.header.pid) > 0) {
            this->pid_stats[this->packet.header.pid]++;
        } else {
            this->pid_stats[this->packet.header.pid] = 1;
        }
    }
}

/*
 * @brief Print header to output file.
 */
void
Stream::print_header(void)
{
    if (this->header_printed == false) {
        this->output.file << "Network name: " << this->nit.network_name << std::endl;
        this->output.file << "Network ID: " << this->nit.network_id << std::endl;
        this->output.file << "Bandwidth: " << this->nit.bandwidth << std::endl;
        this->output.file << "Constellation: " << this->nit.constellation << std::endl;
        this->output.file << "Guard interval: " << this->nit.guard_interval << std::endl;
        this->output.file << "Code rate: " << this->nit.code_rate << std::endl;
        this->output.file << std::endl;
        this->header_printed = true;
    }
}

/*
 * @brief Counts program bandwidth.
 * @return Program bandwidth as double.
 */
double
Stream::count_bandwidth(uint16_t program_pid)
{
    if (this->nit.section.size() == 0) {
        throw Error{"NIT is empty but should be filled at this point!", EIFILE};
    }
    if (this->nit.bandwidth_num != 0x00) {
        throw Error{"Network bandwidth is " + bandwidth_table[this->nit.bandwidth_num] +
                " (" + std::to_string(this->nit.bandwidth_num) +
                ") but expected 8 MHz!", EIFILE};
    }
    uint64_t packet_sum = 0;
    /* Count packets from all channels (audio/video/... tracks) in this program */
    for (auto& pid: this->pmts[program_pid].pids) {
        packet_sum += this->pid_stats[pid];
    }
    /* Count also PMT tables for this program */
    packet_sum += this->pid_stats[program_pid];
    double stream_bandwidth = bandwidth_8MHz_table[this->nit.constellation_num][this->nit.code_rate_num][this->nit.guard_interval_num];
    return ((double)packet_sum) / this->packets_count * stream_bandwidth;
}

/*
 * @brief Parses information about all programs.
 */
void
Stream::get_program_infos(void)
{
    if (this->pmts.size() == 0 ||
            this->pat.programs.size() == 0 ||
            this->sdt.services.size() == 0 ||
            this->pid_stats.size() == 0) {
        BMS2_DEBUG_MSG("PMTs size:         %lu", this->pmts.size());
        BMS2_DEBUG_MSG("PAT programs size: %lu", this->pat.programs.size());
        BMS2_DEBUG_MSG("SDT services size: %lu", this->sdt.services.size());
        BMS2_DEBUG_MSG("PIS stats size:    %lu", this->pid_stats.size());
        return;
    }

    for (auto& program: this->pat.programs) {
        if (program.pid == this->nit_pid) {
            /* Skip NIT table - PID of NIT is also in this list of programs */
            continue;
        }
        ProgramInfo program_info;
        program_info.service_provider = this->sdt.services[program.number].provider;
        program_info.service_name = this->sdt.services[program.number].name;
        program_info.bandwidth = this->count_bandwidth(program.pid);
        this->program_infos[program.pid] = program_info;
    }
}

/*
 * @brief Prints information about all programs to output file.
 */
void
Stream::print_program_infos(void)
{
    for (auto& pi: this->program_infos) {
        std::stringstream pid_stream;
        pid_stream << std::hex << std::setw(4) << std::setfill('0') << pi.first;
        std::stringstream bw_stream;
        bw_stream << std::fixed << std::setprecision(2) << pi.second.bandwidth;
        std::string output_string = "0x" + pid_stream.str() + "-" + pi.second.service_provider + "-" + pi.second.service_name + ": " + bw_stream.str() + " Mbps";
        this->output.file << output_string << std::endl;
    }
}
