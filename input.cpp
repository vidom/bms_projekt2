/*
 * @file
 * @brief BMS 2016/2017 Projekt 2
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date 12/2016
 */

/* Local header */
#include "bms2.h"

using namespace bms2;

/*
 * @brief Input contructor.
 */
Input::Input(std::string fn) :
    filename{fn},
    file{fn, std::ios::binary}
{
    if (!file.is_open()) {
        throw Error{"Could not open input file: " + fn, EIFILE};
    }
    BMS2_DEBUG_MSG("Input file successfully opened!");
}

/*
 * @brief Reset position in input file to beginning.
 */
void
Input::reset(void)
{
    this->file.clear();
    this->file.seekg(0, std::ios::beg);
}
