/*
 * @file
 * @brief BMS 2016/2017 Projekt 2
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date 12/2016
 *
 * Demultiplexing a analyza transportneho streamu vysielania digitalnej
 * televizie.
 */

#ifndef __BMS2_H__
#define __BMS2_H__

/* Global headers */
#include <cstdio>
#include <string>
#include <exception>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <vector>
#include <map>
#include <arpa/inet.h>

#ifdef BMS2_DEBUG
#define BMS2_DEBUG_MSG(...) \
    do { \
        fprintf(stderr, "DEBUG|%s:% 4d:%s()| ", __FILE__, __LINE__, __func__); \
        fprintf(stderr, __VA_ARGS__); \
        fprintf(stderr, "\n"); \
    } while (0)
#else
#define BMS2_DEBUG_MSG(...)
#endif

#define BMS2_PACKET_LENGTH 188
#define BMS2_HOST_TO_BIG32(addr) (htonl(*((uint32_t *)(addr))))
#define BMS2_HOST_TO_BIG16(addr) (htons(*((uint16_t *)(addr))))

namespace bms2 {
    extern std::string bandwidth_table[8];
    extern std::string constellation_table[4];
    extern std::string code_rate_table[8];
    extern std::string guard_interval_table[4];
    extern double bandwidth_8MHz_table[4][8][4];

    enum error_codes {
        EOK = 0,
        EANY = 1,
        EARG = 2,
        EINFN = 3,
        EIFILE = 4,
        EOFILE = 5,
    };

    struct Error : std::exception {
        std::string emsg;
        int ecode;

        Error(const char *msg, int code) :
            emsg{msg},
            ecode{code}
        {}
        Error(const char *msg) :
            emsg{msg},
            ecode{EANY}
        {}
        Error(std::string msg, int code) :
            emsg{msg},
            ecode{code}
        {}
        Error(std::string msg) :
            emsg{msg},
            ecode{EANY}
        {}
        const char *what(void) const noexcept
        {
            std::string ret = "Error (" + std::to_string(this->ecode) + "): " + this->emsg;
            return ret.c_str();
        }
    };

    class Params {
        static const std::string in_suffix;
        static const std::string out_suffix;
      public:
        std::string in_fn;
        std::string fn;
        std::string out_fn;

        Params(int argc, char *argv[]);
      private:
        void print_help(char *argv[]);
    };

    struct PacketHeader {
        uint8_t sync;
        uint8_t tei;
        uint8_t pusi;
        uint8_t tp;
        uint16_t pid;
        uint8_t tsc;
        uint8_t afc;
        uint8_t cc;
    };

    struct Packet {
        uint8_t bytes[BMS2_PACKET_LENGTH];
        PacketHeader header;

        void parse_header(void);
        bool get_payload_offset(uint16_t &payload_start);
    };

    struct Descriptor {
        uint8_t tag;
        uint8_t length;
    };

    struct NIT {
        std::vector<uint8_t> section;
        std::string network_name;
        uint16_t network_id;
        std::string bandwidth;
        std::string constellation;
        std::string guard_interval;
        std::string code_rate;
        uint8_t bandwidth_num;
        uint8_t constellation_num;
        uint8_t guard_interval_num;
        uint8_t code_rate_num;
    };

    struct Program {
        uint16_t number;
        uint16_t pid;
    };

    struct PAT {
        std::vector<uint8_t> section;
        std::vector<Program> programs;
    };

    struct Service {
        std::string provider;
        std::string name;
    };

    struct SDT {
        std::vector<uint8_t> section;
        std::map<uint16_t, Service> services;
    };

    struct PMT {
        std::vector<uint8_t> section;
        std::vector<uint16_t> pids;
    };

    struct ProgramInfo {
        std::string service_provider;
        std::string service_name;
        double bandwidth;
    };

    struct Input {
        std::string filename;
        std::ifstream file;

        Input(std::string fn);
        void reset(void);
    };

    struct Output {
        std::string filename;
        std::ofstream file;

        Output(std::string fn);
    };

    struct Stream {
        Input input;
        Output output;
        uint16_t nit_pid;
        PAT pat;
        NIT nit;
        SDT sdt;
        std::map<uint16_t, PMT> pmts;
        std::map<uint16_t, uint64_t> pid_stats;
        uint64_t packets_count;
        std::map<uint16_t, ProgramInfo> program_infos;

        Packet packet;
        bool header_printed;

        Stream(std::string in_fn, std::string out_fn);
        bool read_packet(void);
        void find_pat(void);
        void find_nit(void);
        void find_sdt(void);
        void find_pmts(void);
        void get_pid_stats(void);
        void get_program_infos(void);
        double count_bandwidth(uint16_t program_pid);
        void print_header(void);
        void print_program_infos(void);
    };

}

#endif
