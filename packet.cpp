/*
 * @file
 * @brief BMS 2016/2017 Projekt 2
 * @author Matej Vido, xvidom00@stud.fit.vutbr.cz
 * @date 12/2016
 */

/* Local header */
#include "bms2.h"

using namespace bms2;

/*
 * @brief Get information about packet header from packet's bytes to header item.
 */
void
Packet::parse_header(void)
{
    uint32_t h = BMS2_HOST_TO_BIG32(&this->bytes[0]);
    this->header.sync = (h & 0xff000000) >> 24; /**< Sync Byte (0x47 == 'G') */
    this->header.tei  = (h & 0x00800000) >> 23; /**< Transport Error Indicator */
    this->header.pusi = (h & 0x00400000) >> 22; /**< Payload Unit Start Indicator */
    this->header.tp   = (h & 0x00200000) >> 21; /**< Transport Priority */
    this->header.pid  = (h & 0x001fff00) >>  8; /**< Packet Identifier (PID) */
    this->header.tsc  = (h & 0x000000c0) >>  6; /**< Transport Scrambling Control */
    this->header.afc  = (h & 0x00000030) >>  4; /**< Adaptation Field Control */
    this->header.cc   = (h & 0x0000000f);       /**< Continuity Counter */
}

/*
 * @brief Gets offset of packet payload.
 * @return true If payload follows the header.
 *         false If there is not any payload.
 */
bool
Packet::get_payload_offset(uint16_t &payload_start)
{
    payload_start = 4;
    if (this->header.afc == 0x00) {
        /* Reserved use, should not happen - skip */
        return false;
    } else if (this->header.afc == 0x01) {
        /* no adaptation field, payload only */
    } else if (this->header.afc == 0x02) {
        /* adaptation field only, no payload - skip */
        return false;
    } else if (this->header.afc == 0x03) {
        /*
         * adaptation field followed by payload - count payload start
         * add Adaptation Field Length
         */
        payload_start += this->bytes[4];
        if (payload_start >= BMS2_PACKET_LENGTH) {
            /* Should not happen */
            BMS2_DEBUG_MSG("Payload starts outside of packet (skip)!!!");
            return false;
        }
    }
    return true;
}
